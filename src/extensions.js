Date.prototype.toHtmlInputFormat = function() {
    return `${this.getFullYear()}-${this.getMonth() + 1 >= 10 ? '' : '0'}${this.getMonth() + 1}-${this.getDate() >= 10 ? '' : '0'}${this.getDate()}`;
};
