require('!style-loader!css-loader!../node_modules/normalize.css/normalize.css')

import Vue from 'vue'
import Extensions from './extensions.js'
import App from './components/App.vue'

new Vue({
    el: '#app',
    render: h => h(App)
})
